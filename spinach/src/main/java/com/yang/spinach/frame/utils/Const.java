package com.yang.spinach.frame.utils;

/**
 * 公用常量
 * 
 * @author yang
 *
 */
public class Const {
	/**
	 * 默认验证码参数名称
	 */
	public static final String DEFAULT_CAPTCHA_PARAM = "captcha";
}
