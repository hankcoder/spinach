<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="path" value="${pageContext.request.contextPath }"></c:set>
<link href="${path }/static/css/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="${path }/static/css/bootstrap/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
<link href="${path }/static/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="${path }/static/css/style-metro.css" rel="stylesheet" type="text/css"/>
<link href="${path }/static/css/style.css" rel="stylesheet" type="text/css"/>
<link href="${path }/static/css/style-responsive.css" rel="stylesheet" type="text/css"/>
<link href="${path }/static/css/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="${path }/static/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="${path }/static/css/select2_metro.css" />
<link rel="stylesheet" type="text/css" href="${path }/static/css/chosen.css" />
<link rel="shortcut icon" href="${path }/static/image/favicon.ico" />
<link href="${path }/static/css/login.css" rel="stylesheet"
	type="text/css" />

<!-- END PAGE LEVEL STYLES -->
