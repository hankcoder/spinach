<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="path" value="${pageContext.request.contextPath }"></c:set>
<!-- BEGIN FOOTER -->
<div class="footer">
	<div class="footer-inner">2015 &copy; Metronic by
		keenthemes.Collect</div>
	<div class="footer-tools">
		<span class="go-top"> <i class="icon-angle-up"></i>
		</span>
	</div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
	<script src="${path }/static/js/jquery/jquery-1.10.1.min.js" type="text/javascript"></script>
<script src="${path }/static/js/jquery/jquery.twbsPagination.min.js"  type="text/javascript"></script>
<script src="${path }/static/js/jquery/jquery-migrate-1.2.1.min.js"
	type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="${path }/static/js/jquery/jquery-ui-1.10.1.custom.min.js"
	type="text/javascript"></script>
<script src="${path }/static/js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<!--[if lt IE 9]>
	<script src="${path }/static/js/excanvas.min.js"></script>
	<script src="${path }/static/js/respond.min.js"></script>  
	<![endif]-->
<script src="${path }/static/js/jquery/jquery.slimscroll.min.js"
	type="text/javascript"></script>
<script src="${path }/static/js/jquery/jquery.blockui.min.js"
	type="text/javascript"></script>
<script src="${path }/static/js/jquery/jquery.cookie.min.js"
	type="text/javascript"></script>
<script src="${path }/static/js/jquery/jquery.uniform.min.js"
	type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="${path }/static/js/select2.min.js"></script>
<script type="text/javascript"
	src="${path }/static/js/jquery/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${path }/static/js/DT_bootstrap.js"></script>
<script type="text/javascript" src="${path }/static/js/jquery/jquery.validate.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="${path }/static/js/app.js"></script>

